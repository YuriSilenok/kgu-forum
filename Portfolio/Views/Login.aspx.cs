﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Portfolio.Tables;

namespace Portfolio.Views
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void regUserGo(object sender, EventArgs e)
        {
            Response.Redirect("~/Views/regUser.aspx");
        }
        protected void butLogin_Click(object sender, EventArgs e)
        {
            //Если в БД нет пользователей то создать стандартный пользователей
            if (Tables.User.Count() == 0) Tables.User.InitDefault();

            Tables.User.RowUser user = Tables.User.Login(tbLogin.Text, tbPass.Text);

            if(user==null)
            {
                //вывести текст ошибки
                labInfo.Text = "пользователь не найден";
            }
            else
            {
                Session["user"] =  user;
                Response.Redirect("~/Views/User/Main.aspx");
            }

        }
    }
}