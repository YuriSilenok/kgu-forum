﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portfolio.DTO;

namespace Portfolio.DAL
{
    public class IndicatorRepository
    {
        //CRUD

        //Create
        //Read - List Find
        //Update
        //Delete

        public List<IndicatorDTO> List()
        {
            List<IndicatorDTO> resultList = new List<IndicatorDTO>();

            resultList.Add(new IndicatorDTO()
            {
                IndicatorId = 1,
                Name = "показатель 1",
                Max = 1
            });

            resultList.Add(new IndicatorDTO()
            {
                IndicatorId = 2,
                Name = "показатель 2",
                Max = 2
            });

            return resultList;
        }
    }
}