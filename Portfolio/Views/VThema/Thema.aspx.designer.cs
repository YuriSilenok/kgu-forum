﻿//------------------------------------------------------------------------------
// <автоматически создаваемое>
//     Этот код создан программой.
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода. 
// </автоматически создаваемое>
//------------------------------------------------------------------------------

namespace Portfolio.Views.VThema {
    
    
    public partial class Thema {
        
        /// <summary>
        /// form1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;
        
        /// <summary>
        /// PNameThema элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PNameThema;
        
        /// <summary>
        /// Label4 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label4;
        
        /// <summary>
        /// PContents элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel PContents;
        
        /// <summary>
        /// Panel1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel1;
        
        /// <summary>
        /// Label1 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label1;
        
        /// <summary>
        /// Label2 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label2;
        
        /// <summary>
        /// Label3 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label3;
        
        /// <summary>
        /// Panel2 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel2;
        
        /// <summary>
        /// Label5 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label5;
        
        /// <summary>
        /// Label6 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label6;
        
        /// <summary>
        /// Label7 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label7;
        
        /// <summary>
        /// Panel3 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel Panel3;
        
        /// <summary>
        /// Label8 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label8;
        
        /// <summary>
        /// Label9 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label9;
        
        /// <summary>
        /// Label10 элемент управления.
        /// </summary>
        /// <remarks>
        /// Автоматически создаваемое поле.
        /// Для изменения переместите объявление поля из файла конструктора в файл кода программной части.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label Label10;
    }
}
