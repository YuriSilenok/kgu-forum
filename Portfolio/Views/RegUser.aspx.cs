﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Portfolio.Tables;

namespace Portfolio.Views
{
    public partial class RegUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
        protected void butLogin_Click(object sender, EventArgs e)
        {

            Session["user"] = Tables.User.Registration(this.tbName.Text, this.tbLogin.Text, this.tbPass.Text);
            Response.Redirect("~/Views/User/Main.aspx");
        }
    }
}