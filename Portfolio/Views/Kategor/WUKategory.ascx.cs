﻿using Portfolio.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Portfolio.Views.Kategor
{
    public partial class WUKategory : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Kategory.Count()==0)
                Kategory.InitDefault();

            if (BulletedList2.Nodes.Count == 0)
            {
                TreeNodeCollection tnc = Kategory.ToTreeNodeCollection();
                for (int i = 0; i < tnc.Count; i++)
                    BulletedList2.Nodes.Add(tnc[i]);
            }
            if (!Page.IsPostBack)
            {
                if (Kategory.Count() == 0) Kategory.InitDefault();
                ListItem[] lic = Kategory.ToListItemCollection();
                DropDownList1.Items.Clear();
                DropDownList1.Items.AddRange(lic);
            }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            Tables.User.RowUser user = (Tables.User.RowUser)Session["User"];///получаем из сессии авторизованного пользователя
            if (user != null)///Добавлять темы могут авторизованные пользователи
            {
                ListItem li = DropDownList1.SelectedItem;///получаем выбранный элемент выпадающего списка
                int idSelRowKat = int.Parse(li.Value);///получаем ид выбранной категории
                Kategory.RowKategory kat = new Kategory.RowKategory(idSelRowKat);///создали временную запись категории
                int idAddThema = Thema.Count() + 1;///создаем ИД добваляемой темы
                Thema.RowThema th = new Thema.RowThema(idAddThema);///создаем запись тема
                th.Name = TB_Thema.Text;///задаем имя темы
                Thema.Add(th);///Добавляем тему в таблицу тем
                Kategory.AddThema(kat, th);///добавляем тему к категории
                Response.Redirect("~/Views/User/Main.aspx");///переход на страницу
            }
        }
    }
}