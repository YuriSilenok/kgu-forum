﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portfolio.Tables
{
    public class Thema : Table
    {
        protected static List<RowThema> Rows = new List<RowThema>();
        public class RowThema : Row
        {
            public string Name { get; set; }
            public RowThema(int id) : base(id) { }
        }


        public static int Count()
        {
            return Rows.Count();
        }

        public static void Add(RowThema th)
        {
            foreach (RowThema row in Rows)
                if (row.ID == th.ID) return;
            Rows.Add(th);
        }
        /// <summary>
        /// SELECT * FROM Thema WHERE ID = id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static RowThema GetThema(int id)
        {
            foreach (RowThema row in Rows)
                if (row.ID == id) return row;

            return null;
        }
    }
}