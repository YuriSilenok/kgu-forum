﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portfolio.Tables
{
    /// <summary>
    /// Таблица с пользователями
    /// </summary>
    public class User : Table
    {
        protected static List<RowUser> Rows = new List<RowUser>();
        /// <summary>
        /// Запись в таблице пользователей
        /// </summary>
        public class RowUser : Row
        {
            public string Login { get; set; }
            public string Password { get; set; }
            public string Name { get; set; }
            public Role.RowRole Role { get; set; }
            public RowUser(int id) : base(id) { }
        }
        /// <summary>
        /// Инициализирует пользователей по умолчанию
        /// </summary>
        public static void InitDefault()
        {
            Rows = new List<RowUser>();
            RowUser user = new RowUser(1)
            {
                Name = "Петров",
                Login = "admin",
                Password = "admin",
                Role = new Role.RowRole(1)
            };
            Rows.Add(user);

            user = new RowUser(2)
            {
                Name = "Иванов",
                Login = "user",
                Password = "user",
                Role = new Role.RowRole(2)
            };
            Rows.Add(user);
       }

        public static RowUser Registration(string name, string login, string password)
        {
            RowUser user = new RowUser(Rows.Count+1)
            {
                Name = name,
                Login = login,
                Password = password,
                Role = new Role.RowRole(2)
            };
            Rows.Add(user);
            return user;
        }

        public static RowUser Login(string login, string password)
        {
            RowUser user = (from userRow in Rows
                            where userRow.Login == login
                           && userRow.Password == password
                            select userRow).FirstOrDefault();
            return user;
        }
        /// <summary>
        /// Возвращает количество записей в таблице
        /// </summary>
        /// <returns></returns>
        public static int Count()
        {
            return Rows.Count;
        }

        public static RowUser GetUser(int id)
        {
            foreach (var item in Rows)
            {
                if (item.ID == id) return item;
            }
            return null;
        }
    }
}