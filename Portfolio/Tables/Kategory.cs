﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Portfolio.Tables
{
    public class Kategory : Table
    {
        protected static List<RowKategory> Rows = new List<RowKategory>();
        public class RowKategory : Row
        {
            public string Name { get; set; }
            public List<Thema.RowThema> Theme { get; set; }
            public RowKategory(int id):base(id)
            {
                Theme = new List<Thema.RowThema>();
            }
        }
      
        public static void InitDefault()
        {
            
            for (int i = 0; i < 3; i++)
            {
                RowKategory kRow = new RowKategory(Rows.Count + 1) { Name = "Категория " + i };
                for (int j = 0; j < 3; j++)
                {
                    Thema.RowThema tRow = new Thema.RowThema(i * 3 + j);
                    tRow.Name = "Тема " + i + " " + j;
                    kRow.Theme.Add(tRow);
                    Thema.Add(tRow);
                }
                Rows.Add(kRow);
            }
        }
        public static TreeNodeCollection ToTreeNodeCollection()
        {
            TreeNodeCollection tnc = new TreeNodeCollection();
            foreach (RowKategory item in Rows)
            {
                TreeNode TNKategory = new TreeNode();
                TNKategory.Text = item.Name;

                foreach (Thema.RowThema item2 in item.Theme)
                {
                    TreeNode TNThema = new TreeNode();
                    TNThema.Text = item2.Name;
                    TNKategory.ChildNodes.Add(TNThema);
                }
                tnc.Add(TNKategory);
            }
            return tnc;
        }
        public static ListItem[] ToListItemCollection()
        {
            ListItem[] tnc = new ListItem[Rows.Count]; ;
            for (int i = 0; i < Rows.Count; i++)
            {
                tnc[i] = new ListItem();
                tnc[i].Text = Rows[i].Name;
                tnc[i].Value = Rows[i].ID.ToString();
            }
            return tnc;
        }

        public static int Count()
        {
            return Rows.Count();
        }

        public static void Add(RowKategory rowKategory)
        {
            foreach (RowKategory item in Rows)
                if (item.ID == rowKategory.ID) return;
            rowKategory.Theme = new List<Thema.RowThema>() { new Thema.RowThema(0){Name ="test"} };
            Rows.Add(rowKategory);
        }



        internal static void AddThema(RowKategory kat, Thema.RowThema th)
        {
            foreach (RowKategory row in Rows)
                if (row.ID == kat.ID)
                {
                    row.Theme.Add(th);
                    break;
                }
        }
    }
}